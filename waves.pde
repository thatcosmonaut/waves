import ddf.minim.spi.*;
import ddf.minim.signals.*;
import ddf.minim.*;
import ddf.minim.analysis.*;
import ddf.minim.ugens.*;
import ddf.minim.effects.*;

PShader waves;
PGraphics pg;
Minim minim;  
AudioPlayer song;
FFT fft;
BeatDetect beat;
float beatValue;

int[] freqs = {60, 170, 310, 600, 1000, 3000, 6000, 12000, 14000, 16000};
float[] current_ffts = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001};
float[] previous_ffts = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001};
float[] max_ffts = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001};
float[] scaled_ffts = {0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001, 0.001};

float fft_smoothing;

void setup() {
  size(1440, 900, P3D);
  pg = createGraphics(640, 360, P3D);
  
  minim = new Minim(this);

  setupSound();
  
  waves = loadShader("waves.glsl");
  waves.set("resolution", float(640), float(360));
  pg.shader(waves);
}

void draw() {
  updateSound(); 
  
  if (beat.isOnset()) 
    beatValue = 0.1;
  else
    beatValue = 0;
  
  waves.set("time", millis() / 1000.0);
  waves.set("fft_high", scaled_ffts[8]);
  waves.set("fft_low", scaled_ffts[3]);
  waves.set("beatValue", beatValue);
  
  pg.beginDraw();
  pg.noStroke();
  pg.fill(0);
  pg.rect(0, 0, 640, 360);
  pg.endDraw();
  image(pg, 0, 0, 1440, 900);
  println(frameRate);
}

void setupSound(){
  song = minim.loadFile("Synkro.mp3", 2048);
  song.loop();
  
  fft = new FFT(song.bufferSize(), song.sampleRate());
  beat = new BeatDetect();
 
  fft_smoothing = 0.9;
}

void updateSound(){
  fft.forward(song.mix);
  previous_ffts = current_ffts;
  
  float new_fft;
  
  for (int i = 0; i < freqs.length-1; i++){
    new_fft = fft.getFreq(freqs[i]); 
    
    if (new_fft > max_ffts[i])
      max_ffts[i] = new_fft;
    
    current_ffts[i] = ((1 - fft_smoothing) * new_fft) + (fft_smoothing * previous_ffts[i]);
    
    scaled_ffts[i] = (current_ffts[i]/max_ffts[i]);
  }
  
  beat.detect(song.mix);
}

void stop()
{
  // always close Minim audio classes when you are done with them
  song.close();
  // always stop Minim before exiting
  minim.stop();
 
  super.stop();
}
